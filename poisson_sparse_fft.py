"""This program shows a comparative between three different
methods to compute the electric potential and the radial component
of the electric field in cylindrical coordinates for a uniformly charged
sphere placed in the middle point between two parallel plates. Two of the
methods use a sparse solver for Poisson's equation and the third
is a semianalytic approach based on the Method Of Images (MOI). The MOI
solution is the considered the most accurate solution. The methods
using the sparse linear solver (from SciPy) differ in the boundary
conditions at rmax, which are Neumann BC and what we call Free BC,
imposed by applying a Domain Decomposition Method (DDM)."""

import numpy as np
import scipy.constants as co
from scipy import sparse
from scipy.sparse import linalg
from matplotlib import pyplot as plt
from scipy import interpolate
from scipy import special as sp
import scipy.fftpack as fft

BC_COEFF = {'neu': 1, 'dir': -1}
DEFAULT_BC = ['dir', 'dir', 'neu', 'dir']



def main():

    nz, nr = 1000, 500
    lz, lr = 10.e-3, 5.e-3
    dz, dr = lz / nz, lr / nr

    # r axis points
    rf = np.linspace(0, lr, nr + 1) # Edges
    rc = 0.5 * (rf[1:] + rf[:-1]) # Centres

    # z axis points
    zf = np.linspace(0, lz, nz + 1)
    zc = 0.5 * (zf[1:] + zf[:-1])

    # Charge density (rho), total charge (q) and right hand side (rhs)
    # of Poisson's equation
    # Charged solid sphere : 3 mm
    width = 3.e-3
    rhodzdr = co.e * 1.e13 * ((zc[:,None] - 5.e-3)**2 + rc[None,:]**2 < width**2) * dr * dz
    q = np.sum(rhodzdr * 2. * np.pi * rc[None,:])
    rhs = rhodzdr / co.epsilon_0


    # Neumann boundary conditions at rmax (lr)
    bc = ['dir', 'dir', 'neu', 'neu']
    mat = compute_matrix(nz, nr, lz, lr, bc)
    phi_NEU = linalg.spsolve(mat, np.ravel(rhs)).reshape(rhs.shape)

    # Free boundary conditions
    phi_DDM = DDM(nz, nr, lz, lr, rhs)

    # Semianalytic computation -  Method of Images
    phi_IMG=MOI(q, lz, zc[:,None], rc[None,:], width)


    # Find out middle point and its array index
    zmed = np.amax(zc) / 2.
    for i, val in enumerate(zc):
        if (zmed - dz < zc[i] and zc[i] < zmed + dz):
            index = i


    # Comparative plots
    # Potential(r)
    plt.title('Potential of a charged solid sphere R=3mm')
    plt.xlabel("r(mm)")
    plt.ylabel("$\phi$(V)")

    plt.plot(rc*1000, phi_IMG[index], label="$\phi$ (Images)")
    plt.plot(rc*1000, phi_DDM[index], '--', label="$\phi$ (Free B.C.)")
    plt.plot(rc*1000, phi_NEU[index], '--', label="$\phi$ (Neumann B.C.)")

    plt.grid()
    plt.legend()

    plt.savefig("phi.png", dpi=600)
    plt.clf()

    # Er(r)
    Er_IMG = - np.gradient(phi_IMG,dr,axis=1)
    Er_DDM = - np.gradient(phi_DDM,dr,axis=1)
    Er_NEU = - np.gradient(phi_NEU,dr,axis=1)

    plt.title('Radial electric field of a charged solid sphere R=3mm')
    plt.xlabel("r(mm)")
    plt.ylabel("$E_r$(V/m)")

    plt.plot(rc*1000, Er_IMG[index], label="$E_r$ (Images)")
    plt.plot(rc*1000, Er_DDM[index], '--', label="$E_r$ (Free B.C.)")
    plt.plot(rc*1000, Er_NEU[index], '--', label="$E_r$ (Neumann B.C.)")

    plt.grid()
    plt.legend()

    plt.savefig('Er.png', dpi=600)



def apply_inhom_bc(rhs, b, nx, ny, lx, ly):
    """ Modifies rhs to apply an inhomogeneous b.c. at the external side of the
    cylinder.  """
    #
    # We have to use that when j + 1 falls outside the domain
    # phi[i, j + 1] = 2 b[i] - phi[i, j]  (Dirichlet b.c)
    # This adds a contribution to q[i, j] equal to 2 b[i] times the
    # coefficient that would multiply phi[i, j + 1] (i.e. di=0, dj=1) in the
    # stencil defined in compute_matrix.  This is -HydHx - 0.5 * Hx / r.
    Hx, Hy    = lx / nx, ly / ny
    HxdHy     = Hx / Hy
    HydHx     = Hy / Hx
    r = (ny - 0.5) * Hy

    rhs[:, -1] -= 2 * (-HxdHy - 0.5 * Hx / r) * b


    return rhs



def compute_matrix(nx, ny, lx, ly, bc=None):
    if bc is None:
        bc = DEFAULT_BC

    Hx, Hy = lx / nx, ly / ny
    HxdHy     = Hx / Hy
    HydHx     = Hy / Hx

    # The dok (dictionary-of-keys) format is efficient to construct the matrix
    # but very inefficient to use it.
    mat = sparse.dok_matrix((nx * ny, nx * ny))

    for i in range(nx):
        for j in range(ny):
            r = (j + 0.5) * Hy

            stencil =  ((-HxdHy,               0,  -1),
                        (-HydHx,              -1,   0),
                        (2.0*(HxdHy + HydHx),  0,   0),
                        (-HydHx,              +1,   0),
                        (-HxdHy,               0,  +1),
                        (-0.5*Hx/r,            0,  +1),
                        (0.5*Hx/r,             0,  -1))

            row = [i, j]
            for (v, di, dj) in stencil:
                col = [i + di, j + dj]

                # Apply boundary conditions
                if i == 0 and di == -1:
                    col[0] = i
                    v = BC_COEFF[bc[0]] * v

                if i == nx - 1 and di == 1:
                    col[0] = i
                    v = BC_COEFF[bc[1]] * v

                if j == 0 and dj == -1:
                    col[1] = j
                    v = BC_COEFF[bc[2]] * v

                if j == ny - 1 and dj == 1:
                    col[1] = j
                    v = BC_COEFF[bc[3]] * v

                # Unroll indices
                nrow = row[0] * ny + row[1]
                ncol = col[0] * ny + col[1]

                prev = mat.get((nrow, ncol), 0.0)

                mat[nrow, ncol] = prev + v


    return mat.tocsr()



def DDM(nz, nr, lz, lr, rhs):

    dz = lz / nz
    dr = lr / nr

    rf = np.linspace(0, lr, nr + 1)
    rc = 0.5 * (rf[1:] + rf[:-1])

    zf = np.linspace(0, lz, nz + 1)
    zc = 0.5 * (zf[1:] + zf[:-1])


    # We need to solve Poisson's equation twice according to the DDM
    mat = compute_matrix(nz, nr, lz, lr)

    phi = linalg.spsolve(mat, np.ravel(rhs)).reshape(rhs.shape) # 1

    dphi_drho = np.gradient(phi, dr, axis=1)

    # We set a b.c. at rmax (phi_gamma) that is compatible with the b.c. at zmin and zmax.
    ############################################################################
    ############################################################################

    dphi_interp = interpolate.RectBivariateSpline(zc,rc,dphi_drho)

    # "Flux equation"
    N = nz # Number of points in the k-space
    km =  np.pi * np.arange(1, N + 1) /lz
    arg = km * lr

    factor = sp.i1e(arg) / sp.i0e(arg) + sp.k1e(arg)/sp.k0e(arg)
    factor = factor * km * lz / 2.

    dphi_drho =  dphi_interp.ev(zc,rf[-1])

    bm = - 0.5 * fft.dst(dphi_drho, type=2)*dz

    am = bm / factor

    am_aux = np.zeros(am.shape)
    am_aux[-1] = am[-1]

    phi_gamma =  0.5 * fft.dst(am,type=3) + fft.dst(am_aux,type=3)

    ############################################################################
    ############################################################################

    # Modification of the r.h.s in order to include the BC at rmax
    rhs = apply_inhom_bc(rhs, phi_gamma, nz, nr, lz, lr)

    phi = linalg.spsolve(mat, np.ravel(rhs)).reshape(rhs.shape) # 2

    return phi



def MOI(q, lz, z, r, Rs):
    # Calculation of the electric potential generated by an uniformly charged sphere
    #placed at the middle point between two plates using the Method of Images

    # Number of images
    n_images = 1000
    n_i = np.arange(-n_images,n_images + 1,1)

    # Common prefactor
    prefactor = q /(4.*np.pi*co.epsilon_0)

    # Initializing potential array
    phi_i = np.zeros(q.shape)
    z0 = lz/2. # z coordinate of the middle point

    for count in n_i:
        if count!= 0:
            count = int(count)
            prefactor_sign =  (-1)**count * prefactor

            # Distance to the middle point between the two plates
            vshift_centre = 2 * count * (lz - z0)

            # Vector joining the centre of every sphere with the point (r,z)
            r_sph_shift = np.sqrt(r**2 + (z - z0 - vshift_centre)**2)

            # Outer potential (except prefactor) generated by every sphere
            # except that one between the plates
            phi_i = phi_i + prefactor_sign / r_sph_shift

    r_sph = np.sqrt(r**2 + (z - z0)**2)

    pot_ext = prefactor / r_sph # Outer potential generated by the sphere between plates
    pot_int = prefactor * (3. - r_sph**2 / Rs**2) / (2. * Rs) # Inner potential ""


    phi_i = phi_i + pot_ext * (r_sph**2 > Rs**2) + \
                    pot_int * (r_sph**2 <= Rs**2)


    return phi_i



if __name__ == '__main__':
    main()
